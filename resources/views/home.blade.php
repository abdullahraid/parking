@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-info">
                <div class="panel-heading">Account Info</div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="https://randomuser.me/api/portraits/men/83.jpg" style="max-height: 100px;max-width: 100%;">
                        </div>
                        <div class="col-md-8">
                            <p><strong>Name:</strong> {{ $user->name }}</p>
                            <p><strong>Email:</strong> {{ $user->email }}</p>
                            <p><strong>Linked Cars:</strong> {{ $user->tags()->count() }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-info">
                <div class="panel-heading">Account Balance</div>

                <div class="panel-body">
                    <p>Your current balance is:</p>
                    <div style="font-size: 2em;" class="{{ $balance > 10 ? 'text-success' : 'text-danger' }}">{{ number_format($balance, 2) }} <span style="font-size: 0.5em;">AED</span></div>
                    <small>You will be notified by SMS if it goes low.</small>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-info">
                <div class="panel-heading">Add Balance</div>

                <div class="panel-body">
                    <p>Add balance to your account:</p>
                    <form action="{{ url('/process-payment') }}" method="post">
                        {{ csrf_field() }}
                        <div class="input-group" style="margin-bottom: 0.5em;">
                            <input name="amount" type="text" class="form-control" placeholder="Amount..." aria-label="Search for...">
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit">Pay Now!</button>
                            </span>
                        </div>
                    </form>
                    <small>You will be redirect to the payment gateway afterwards.</small>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">Your Activity</div>

                <div class="panel-body" style="min-height: 130px;">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Plate Number</th>
                            <th scope="col">Parking (Slot)</th>
                            <th scope="col">Slot Type</th>
                            <th scope="col">From</th>
                            <th scope="col">To</th>
                            <th scope="col">Duration</th>
                            <th scope="col">Charge</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($user->payments()->where('amount', '<', 0)->get() as $payment)
                            <tr>
                                <th scope="row">{{ $payment->id }}</th>
                                <td>{{ $payment->tag->plate_number }}</td>
                                <td>{{ $payment->slot->parking->name }} ({{ $payment->slot->id }})</td>
                                <td>{{ $payment->slot->category->type }}</td>
                                <td>{{ $payment->created_at }}</td>
                                <td>{{ $payment->updated_at }}</td>
                                <td>{{ $payment->created_at->diffForHumans($payment->updated_at, true) }}</td>
                                <td>{{ $payment->amount }} AED</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
