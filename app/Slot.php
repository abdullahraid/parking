<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slot extends Model
{
	public function category() {
		return $this->belongsTo(Category::class);
    }

	public function parking() {
		return $this->belongsTo(Parking::class);
    }
}
