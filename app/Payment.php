<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
	public $fillable = ['tag_id', 'amount', 'slot_id', 'user_id'];

	public function tag() {
		return $this->belongsTo(Tag::class);
	}

	public function slot() {
		return $this->belongsTo(Slot::class);
	}
}
