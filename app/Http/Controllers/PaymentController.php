<?php

namespace App\Http\Controllers;

use App\Category;
use App\Payment;
use App\Slot;
use App\Tag;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;

class PaymentController extends Controller
{
    public function create(Request $request)
    {
        $this->validate($request, [
            'uid'  => 'required',
            'slot' => 'required|integer|min:1'
        ]);
        $tag  = Tag::where('uid', $request->get('uid'))->firstOrFail();
        $slot = Slot::findOrFail($request->get('slot'));
        
        $recentPayment = Payment::where([
            ['tag_id', '=', $tag->id],
            ['slot_id', '=', $slot->id],
            ['updated_at', '>=', Carbon::now()->subMinutes(env('PARKING_TIMEOUT'))]
        ])->first();
        
        if ( ! $recentPayment) {
            Payment::create([
                'tag_id'  => $tag->id,
                'slot_id' => $slot->id,
                'user_id' => $tag->user->id,
                'amount'  => 0,
            ]);
        }else{
            $recentPayment->amount = (-1 * $slot->category->price * $recentPayment->created_at->diffInMinutes(Carbon::now(), true));
            $recentPayment->save();
        }
        
        return 'success';
    }
    
    public function processPayment(Request $request)
    {
        $this->validate($request, [
            'amount' => 'numeric|min:0'
        ]);

        Auth::user()->payments()->create([
            'amount' => $request->post('amount')
        ]);
        
        return redirect('/home');
    }
}
